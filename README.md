## 1. What and Why Git

#### Exercise:
- say what you think git is and where did you see/use it

#### Concept:
- version control
- share with team

## 2. Create repository

#### Exercise:
- Create on gitlab/github
- Create in a folder on your computer

#### Concept:
- Every git repository is independant

#### Command:
- `mkdir my_project`
- `git init`

## 3. Create file in GUI

#### Exercice:
- create a file called `first_file.txt` in the GUI
- add content in this file, like "hello world"

#### Concept
- commit
- history

## 4. Link repository

#### Exercise:
- add a remote in you git folder on your computer

#### Concept:
- get content and modification

#### Command
- `git remote`
- `git pull`

## 5. Create file on your computer

#### Exercice:
- create a file called `second_file.txt` in `my_project folder`
- add content in this file, like "I love linux"

#### Concept:
- local modification
- commit message

#### Command:
- `git add`
- `git status`
- `git commit`

## 6. Report modification on Gitlab/Github

#### Exercise:
- Push your local modification on remote

#### Concept
- compare history

#### Command:
- `git push`
- `git status`
- `git log`

## 7. Pause 5 minutes

## 8. Create Branch

#### Exercise:
- create a local `branch my_first_branch`
- Modify a file
- Push the branch on github/gitlab

#### Concept:
- branch tree
- best practice (master/main as ref, branch for bugfix/feature)

#### Command:
- `git checkout`
- `git commit`
- `git push`

## 9. Create Merge Request

#### Exercise:
- in gitlab/github create a merge request for `my_first_branch`
- wait for comment on other person merge request
- get local modif after merge

#### Concept:
- code review
- merge

#### Command:
- `git fetch`
- `git merge`

## 10. Get existing repository

#### Exercise:
- Fork repository in gitlab/github
- Get a local copy

#### Concept:
- fork

#### Command:
- `git clone`
- `git remote`

## 11. Propose change

#### Exercise:
- Create a Local Branch
- Add a file `your_firstname.txt` with content like "I love git"
- Push on your forked repository
- Create a Merge Request

#### Concept:
- share work

#### Comand:
- `git checkout`
- `git add`
- `git commit`
- `git push`

## 12. Update your fork

#### Exercise:
- Update your forked repositiory

#### Concept:
- keep master/main clean

#### Command:
- `git pull`
- `git push`

## 13. Pause 5 minutes

## 14. Bonus if enought time

## Concept:
- work branch
- rebase/reset
